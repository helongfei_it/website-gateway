[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## Website-gateway main capabilities

Website-gateway is used to encapsulate the front-end services of each business platform of EdgeGallery, mainly providing the following capabilities:
- 1 Implement the isolation of the background interface, and the client's access request is forwarded to the background service through the zuul gateway of website-gateway
- 2 Client services that implement single sign-on
- 3 Support northbound interface request forwarding
- 4 Support push notification to client for session invalidation

## How to get started

- 1 Compile the business front-end code and copy the content in the dist directory to /src/main/resource/static of Website-gateway

- 2 The following environment variables need to be configured when starting locally:

    - **SC_ADDRESS**: The address of the connected SC. The default SC running locally is: http://127.0.0.1:30100
    - **CLIENT_ID**: ID of the foreground service to be started. It needs to be consistent with the oauth2.clients.clientId configured in the User Management service
    - **CLIENT_SECRET**: The key of the foreground service to be started. It needs to be consistent with the oauth2.clients.clientSecret configured in the User Management service
    - **AUTH_SERVER_ADDRESS**: URL of User Management service, such as http://x.x.x.x:8067
    - **AUTH_SERVER_ADDRESS_CLIENTACCESS**: This configuration is a variable defined for the proxy access mode. In normal access mode, it can be consistent with AUTH_SERVER_ADDRESS
    - **COOKIE_NAME**: The SESSION ID defined for the corresponding business foreground, for example, the SESSIONID name of the AppStore is APPSTORESESSIONID. You can also not configure it, use the default JSESSIONID

- 3 Local start:

    Run the main function in the /src/main/java/org/edgegallery/website/GatewayApplication.java file to start the corresponding business foreground.

## Use RateLimit-zuul to limit the number of API access

```yaml
zuul:
  routes:
    user-mgmt-be: /mec-usermgmt/**
    mec-developer: /mec-developer/**
    mec-appstore: /mec-appstore/**
    mec-atp: /mec-atp/**
    mec-lab: /mec-lab/**
    mecm-inventory: /mecm-inventory/**
    mecm-appo: /mecm-appo/**
    mecm-apm: /mecm-apm/**
    mecm-north: /mecm-north/**
    mec-thirdsystem: /mec-thirdsystem/**
  sensitive-headers:
  ratelimit:
    enabled: true
    behind-proxy: false
    repository: JPA
    default-policy:
      limit: 10 #optional - request number limit per refresh interval window
      quota: 1000 #optional - request time limit per refresh interval window (in seconds)
      refresh-interval: 30 #default value is 60 (in seconds)
      type:
        - USER
        - URL
        - ORIGIN
```
Response when has error:
```json
{
    "timestamp": "2021-08-04T01:40:54.123+0000",
    "status": 429,
    "error": "Too Many Requests",
    "message": ""
}
````
Rules Description:
  - If a link is accessed more than 10 times within 30 seconds, the next request will be intercepted and a 429 error will be returned
  - Link uniqueness is determined by: username/access IP/access API, for example: rate-limit-application:mec-developer:test123:/mec/developer/v1/projects/:127.0.0.1
    > 1. rate-limit-application: fixed prefix
    > 2. mec-developer: serviceId, which can be other services defined in zuul.routes
    > 3. test123: username to access the link
    > 4. /mec/developer/v1/projects/: the link address being accessed
    > 5. 127.0.0.1: Access source IP, obtained from "X-Forwarded-For" of the request header
  - Use an in-memory database to save intermediate data
  - For more detailed configuration, please refer to: [spring-cloud-zuul-ratelimit:2.1.0.REALSE](https://github.com/marcosbarbero/spring-cloud-zuul-ratelimit/tree/v2.1.0.RELEASE)
 
 ## Compile and package
 
 #### Compile parent dependency repository
 
   - Pull code
     ```
      git clone https://gitee.com/edgegallery/eg-parent.git
     ```
   - install dependencies
     ```
      mvn clean install
     ```
 #### Compile Website-gateway
 
   - Pull code
      ```
       git clone https://gitee.com/edgegallery/website-gateway.git
      ```
   - install dependencies
      ```
       maven clean install
      ```